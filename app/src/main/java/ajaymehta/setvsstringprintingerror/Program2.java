package ajaymehta.setvsstringprintingerror;

import java.util.Set;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program2 {

    public static void main(String args[]) {

        Set<String> hs = new java.util.HashSet<>();

        // add elements in hashSet

        hs.add("One");
        hs.add("Two");
        hs.add("Three");
        hs.add("Four");
        hs.add("Five");


        // one of the better soluton is make print a generic method ..so it will take all type of calculation values from all types of method of hashSet ..add, remove , Boolean value etc..

        print(hs); //  we passing  hashSet to print method  Set<String>

        System.out.println();
        // or even we can pass other type of data...
        print(hs.size());  // lets get the size of hashSet ..it must be in Integer Object.




        // hey like generic saves .our method overloading also saves ...so why dont we use both of them to print
        // all types of primitive and Objects values....cool baby cool..
        //TODO.. go n look up ...project  MethodOverloadingSaves...

    }

    public static <T>void print(T value) {  // method overloading...

        System.out.print(value);

    }
}
