package ajaymehta.setvsstringprintingerror;

import java.util.Set;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program {

    public static void main(String args[]) {

        Set<String> hs = new java.util.HashSet<>();

        // add elements in hashSet

        hs.add("One");
        hs.add("Two");
        hs.add("Three");
        hs.add("Four");
        hs.add("Five");

        // now i have to print hasSet ...
//TODO...uncomment below n see its giving error why ?
      //  print(hs);   // package name mat padna ..its saying  print method takes (String value )  ..but hs value is set<String>

        // so now the question is i thought ki ek method bana dete hai ..jisme sari value Print ho jayengi ...to baar baar
        // System.out.println() likhna nai padega ...
        // see Program2 for solution..


    }

    public static void print(String value) {  // i have to change here from String to object .. it will hold all type of objects...n the work is done..
        // but we have got a other solution too..

        System.out.print(value);

    }



    // this will work..
/*
    public static void print(Object value) {

        System.out.print(value);

    }
*/


}
